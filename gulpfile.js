var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var connect = require('gulp-connect');

gulp.task('connect', function() {
	connect.server({
		root: 'public',
		livereload: true
	});
});

gulp.task('sass', function () {
	return gulp.src('./sass/**/*.scss')
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
		.pipe(sourcemaps.write('./'))
		.pipe(connect.reload())

	.pipe(gulp.dest('./public/css'));
});

gulp.task('livereload', function () {
	return gulp.src('./public/**/*')
		.pipe(connect.reload());
});

gulp.task('watch', function (){
	gulp.watch('./sass/**/*.scss', ['sass']);
	gulp.watch('./public/**/*', ['livereload']);
});

gulp.task('default', ['connect','watch','sass', 'livereload']);